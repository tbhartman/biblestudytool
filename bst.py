import argparse
import sys
import os
import glob
TOOLS_DIR = os.path.join(os.path.dirname(__file__), 'tools')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='abqt')
    subparser = parser.add_subparsers(dest='subparser_name')
    subparsers = {}
    def add_tool(path):
        tool_name = os.path.splitext(os.path.split(path)[-1])[0]
       #module = importlib.import_module('abqtools.tools.'+tool_name)
        module = __import__('tools.'+tool_name,fromlist=[tool_name])
        try:
            help = module.Tool.__doc__.split('\n')[0]
        except AttributeError:
            help = tool_name
        subparsers[tool_name] = subparser.add_parser(tool_name,help=help)
        return tool_name,module
    files = map(lambda p: os.path.split(p)[-1],
                glob.glob(os.path.join(TOOLS_DIR,'*.py')))
    files = list(files)
    files.pop(files.index('__init__.py'))
    tools = dict(map(add_tool,files))
    help_parser = subparser.add_parser('help',help='show help for tool')
    help_parser.add_argument('tool',help='name of tool')
    
    if len(sys.argv) < 2:
        parser.print_help()
        exit()
    args,ukargs = parser.parse_known_args()
    if args.subparser_name == 'help':
        tools[args.tool].Tool.parser.print_help()
        exit()
    
    tool = tools[args.subparser_name].Tool()
    tool.run(ukargs)



