#!/usr/bin/env python2
import argparse
import os
import shutil
import sys
import datetime
from biblestudytool.tools import ToolTemplate

class Tool(ToolTemplate):
    """create plot of study progress"""
    def __init__(self):
        self.parser.add_argument('-o','--output', help='output filename [plot.pdf]',
                                 default='plot.pdf')
        self.accepts_null_arguments = True
    def utility(self,**kwargs):
        plot(kwargs['output'])

def plot(filename):
    print(filename)
    try:
        from matplotlib import pyplot as plt
    except Exception as e:
        print(e)
    finally:
        return
    if len(usefiles) > 1:
        print('WARNING: I am ignoring the following files!')
        usefile = usefiles.pop(0)
        for f in usefiles:
            print('    {:s}'.format(f.filename))
    fig_length = 50./1000. * len(usefile)
    fig = plt.figure(figsize=[fig_length,10])
    ax = fig.add_subplot(111)
    marks = usefile.get_mark()
    wall = usefile.get_walltime()
    wall = usefile.get_cpu()
    x = range(len(marks))
    fig.subplots_adjust(left=0.01,top=0.98,bottom=0.3,right=0.995)
    
    colors = {}
    styles = {}
   #title = filename
   #x = data[filename]['x']
   #x.pop(0)
   #x = np.array(x,dtype='float')
   #x -= 0.5
   #y = data[filename]['y2']
   #y.pop(0)
    style = {'marker':'o'}
    for k,v in colors.iteritems():
        if k in filename:
            style.update(color=v)
    for k,v in styles.iteritems():
        if k in filename:
            style.update(linestyle=v)
    title = 'cpu load'
    style.update({'linewidth':1,'markersize':2,'label':title,'markeredgewidth':0})
    ax.plot(x,wall,**style)



    ax.legend(loc=2)
    ax.grid(True)
    ax.set_ylabel('Time Between Marks (s)')
    ylim = ax.get_ylim()
    ax.set_ylim([-1,ylim[1]])
   #ax.set_ylim([-1,200])
    ax.set_xlim([0,len(marks)-1])
    ax.set_xticks(range(len(marks)))
    junk = ax.set_xticklabels(marks,rotation='vertical')
    # this for loop takes a lot of time...need to pythonize
    for label in ax.get_xticklabels():
        label.set_fontsize(3)
        label.set_rotation('vertical')


    fig.savefig('load.pdf')

Tool.run_as_script()
