"""template for tools"""
import argparse
import sys
class NullUtilityError(Exception): pass

class ToolTemplate(object):
    """Template class for inheritance in tools.

    Ths class serves as a superclass to be inherited by a `Tool` class
    within submodules.  This class is not intended to be
    instantiated.
    
    Attributes
    ----------
    accepts_null_arguments : boolean
        true if tool is useful without arguments (default is False)
    parser : argParse.ArgumentParser
        parser to be used for parsing arguments

    """
    accepts_null_arguments = False
    
    def __new__(cls):
        """Instantiate the class."""
        obj = object.__new__(cls)
        obj.parser = argparse.ArgumentParser()
        return obj
    
    @classmethod
    def run_as_script(cls):
        """Run tool as its own script.

        Call this function from the tool file to run the file as a
        script.  The intention is to run

        >>> Tool.run_as_script()

        from within the tool module.  If run from `__main__`, this will
        run as a script.
        """
        if cls.__module__ == '__main__':
            t = cls()
            t.run(sys.argv[1:])
    
    def utility(self,**kwargs):
        """Action to perform when running the tool.

        This method must be overridden by 
        
        Parameters
        ----------
        **kwargs
           namespace from parser is passed in as keyword arguments

        """
        raise NullUtilityError('Utility method not overridden!')

    def run(self,args):
        """Perform tool action.


        Parameters
        ----------
        args : list
            arguments to be passed to the parser

        """
        if not args and not self.accepts_null_arguments:
            self.parser.print_help()
        else:
            args = self.parser.parse_args(args)
            self.utility(**args.__dict__)


